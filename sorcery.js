$(function () {


    var inIntro = true;


    var buttons = $(".section button");
    $(".section").hide();
    $("#descriptionObjet").hide()

    $(".section#intro").show();
    var life;
    //Initialisation de l'image
    $(".fond img").hide();
    $("#imgintro").show();

    // Definition; de la simili classe Item
    function Item(nom, description, urlImage) {
        this.nom = nom;
        this.despription = description;
        this.urlImage = "img/objets/" + urlImage;
    }

    Item.prototype.getInfo = function () {
        return this.nom + ' ' + this.despription + ' ' + this.urlImage;
    };
    Item.prototype.utiliser = function () {
        console.log("Set me a function !")
    };

// TODO Reinitialiser inventaire à la mort
    function Inventaire() {
        this.capaciteMax = 4;
        this.contenue = [];
        this.nbObjet = 0;
    }

    Inventaire.prototype.ajoutObjet = function (unItem) {
        if (this.nbObjet < this.capaciteMax) {
            this.contenue.push(unItem);
            this.nbObjet++;
        }
        this.redessinnerInventaire()


    };
    Inventaire.prototype.redessinnerInventaire = function () {
        for (var i = 0; i < this.capaciteMax; i++) {
            console.log($("#inventaire .bloc").eq(i))
            console.log(i)
            if (typeof (this.contenue[i]) !== 'undefined')
                $("#inventaire .bloc").eq(i).find("img").attr("src", this.contenue[i].urlImage)
            else
                $("#inventaire .bloc").eq(i).find("img").removeAttr("src");
        }
        Inventaire.prototype.retirerObjet = function (index) {
            this.contenue.splice(index - 1, 1);
            this.nbObjet--
            console.log(this.contenue)
            this.redessinnerInventaire()
        }
    };

    var monInvent = new Inventaire();

    buttons.click(function () {
            // Initialisation de l'inventaire et de la vie
            if ($(this).attr('go') == "wakeUp") {
                startGame();
            }


            if ($(this).attr('load') == "aventure1-5") {
                bonbon = new Item("Un bonbon", "Bonbon de monstre -" +
                    " Soigne de 10 PV - A un goût distinct de \"non-réglisse\".", "bonbon.jpg");
                bonbon.utiliser = function () {
                    console.log(life)

                    heal(10)
                    console.log(life)

                }
                monInvent.ajoutObjet(bonbon)
            }


            // Test si la vie tombe à zéro -> fin, sinon on continue
            if ((life <= 1)) {
                endGame();
            }
            else {
                if ($(this).attr('go')) {
                    gotoSection($(this).attr('go'));
                }
                if ($(this).attr('load')) {
                    loadSection($(this).attr('load'));

                }
            }
            // Perte de 1 PV
            if ($(this).attr('go') == "hitWall" || $(this).attr('go') == "hitDoor" || $(this).attr('go') == "poing") {
                looseOneLife();
            }


            // Gestion des images


            if ($(this).attr('go') == "intro") {
                inIntro = true;
            }
            if ($(this).attr('go') == "intro10") {
                inIntro = false;
            }
            if ($(this).attr('load') == "aventure1-4") {
                monInvent.retirerObjet(1)
            }
            if ($(this).attr('load') == "aventure5-pic") {
                looseOneLife()
            }

            if ($(this).attr('go') != undefined) {
                var attrValue = $(this).attr('go')

                $(".fond img").hide();
                $("#img" + attrValue).show();

            }


        }
    );


    function gotoSection(key) {
        $(".section").hide();
        $("#" + key).show();
    }

    function loadSection(key) {
        $(".section").hide();
        $("#" + key).show();
    }

    function setLife(value) {
        $("span.value").text(value);
        life = value
    }

    function heal(value) {
        console.log({value: value})
        if ((value + life) > 10)
            setLife(10)
        else
            setLife(value + life)
    }

    function looseOneLife() {
        life = life - 1;
        setLife(life);
    }

    function startGame() {
        //Initialisation de la vie
        life = 10;
        $("span.value").text(life);
        $(".life").show();
        //Initialisation de l'inventaire
        $(".knife").hide();

    }

    function endGame() {
        gotoSection('death');
        //Permet de recommencer le jeu en annulant la condition (if life==1)
        life = undefined;
        $(".life").hide();
    }

    $("#inventaire .bloc").click(function () {

        console.log($(this))

        var index = $("#inventaire .bloc").index(this);
        //console.log(index)
        //console.log(monInvent.contenue[index].getInfo())
        if (typeof monInvent.contenue[index] !== 'undefined') {
            var itemClicked = monInvent.contenue[index]
            itemClicked.utiliser()
        }
    });

    $("#inventaire .bloc").hover(
        function () {
            var index = $("#inventaire .bloc").index(this);
            if (typeof (monInvent.contenue[index]) !== 'undefined') {
                $("#descriptionObjet").show()
                $("#descriptionObjet span").html(monInvent.contenue[index].despription)

            }

        },
        function () {
            $("#descriptionObjet").hide()

            $("#descriptionObjet span").html("")

        })


})
;